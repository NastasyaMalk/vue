var app = new Vue({
  el: '#app',
  data: {
    message: '',
    show: false,
    search: '',
    text: '',
    lists: [
      {name: 'Маша', age: 23},
      {name: 'Петя', age: 20},
      {name: 'Кирилл', age: 26},
    ],
    test: '123'
  },
  methods:{
    onClick(){
      console.log(this.message);
      this.message=''
    }
  },
  computed: {
    count(){
      return this.text.length
    }
  }
})
var app1 = new Vue({
  el: '#app1',
  data: {
    expression: 'Ура! Мы научились добавлять описание.',
    description: 'Нужный текст : «пара теплых нечетких носков»'
  }
})

var app2 = new Vue({
  el: '#app2',
  data: {
    google: 'https://www.google.ru/'
  }
})

var app3 = new Vue({
  el:'#app3',
  data:{
    onSale: true
  }
})

var app4 = new Vue({
  el:'#app4',
  data:{
    sizes: ["38", "39", "40", "41", "42"] 
  }

})

var app5 = new Vue({
  el:'#app5',
  data:{
    cart:0,
    has: true,
    image: "1.png",
    lists: [
      {name:"Фото 1", img:"1.png"},
      {name:"Фото 2", img:"2.png"},
    ]
  },
  methods:{
    plus(){
      this.cart+=1;
      if (this.cart === 0) {this.has = true}
      else {this.has = false}
    },
    minus(){
      this.cart-=1;
      if (this.cart === 0) {this.has = true}
      else {this.has = false}
    },
    newFoto (img) {
      this.image = img
    }
  }
})

var app7 = new Vue({
  el:'#app7',
  data:{
    cart:0,
    has: true,
    saleN: 0
  },
  methods:{
    plus(){
      this.cart+=1;
      if (this.cart === 0) {this.has = true}
      else {this.has = false};
      if (this.cart >= 5) {
        this.saleN = this.cart - (this.cart % 5)}
    },
    minus(){
      this.cart-=1;
      if (this.cart === 0) {this.has = true}
      else {this.has = false};
      if (this.cart >= 5) {
        this.saleN = this.cart - (this.cart % 5)}
    }
  },
    computed:{
      saleS(){
        return 'Ваша скидка на ' + this.cart + ' манго составит ' + this.saleN + '%'
      }
    }
  
})